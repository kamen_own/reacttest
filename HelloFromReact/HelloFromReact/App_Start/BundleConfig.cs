﻿using System.Web;
using System.Web.Optimization;
using System.Web.Optimization.React;

namespace HelloFromReact
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new BabelBundle("~/bundles/main").Include(
                "~/scripts/react/react.js", 
                "~/scripts/react/react-dom.js", 
                "~/scripts/react/react-with-addons.js",
                "~/Content/HelloWorld.jsx"));
        }
    }
}
